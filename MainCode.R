setwd("~/Desktop/My Files/Projects/Moringa")

libs <- c("dplyr", "magrittr", "ggplot2", "readr", "caret", "tidyr", "kableExtra",
          "data.table",  "googlesheets", "gridExtra")
install_or_load_pack <- function(pack){
  create.pkg <- pack[!(pack %in% installed.packages()[, "Package"])]
  if (length(create.pkg))
    install.packages(create.pkg, dependencies = TRUE)
  lapply(libs, require, character.only = T, warn.conflicts=T, quietly=T)
  #I know I should be using purr here, but this is before the Tidyverse is loaded. I know you Tidyverse trend setters will have me here.
}
install_or_load_pack(libs)

#gs_auth(new_user = TRUE)

DataAsList <- gs_title("Default Rate")
MainData <- gs_read(DataAsList)

#Save the data frame in the current working directory
write.csv(MainData, file = "Dataset/MainData")

MainData <- read_csv("Dataset/MainData")
MainData <- MainData[,-2]
#Rename Variable
names(MainData)[names(MainData) == 'X1'] <- 'UniqueId'
names(MainData)

###################################################################################
plot1 <- data.frame(table(MainData$default))%>%
            dplyr::rename(Default_status = "Var1", Count = "Freq")%>%
            ggplot(aes(x=Default_status, y=Count, fill = Default_status))+
            xlab("Default Status")+ ylab("Number of People")+ 
            facet_wrap(~"Defaulters Vs NonDefaulters")+
            geom_bar(stat="identity", width=0.4, position=position_dodge(0.7))+
            geom_text(aes(label=Count), vjust=-0.2, colour="black")+
            guides(fill=F)

plot2 <- data.frame(table(MainData$student))%>%
            dplyr::rename(Student_status = "Var1", Count = "Freq")%>%
            ggplot(aes(x=Student_status, y=Count, fill = Student_status))+
            xlab("Student")+ ylab("Number of People")+
            facet_wrap(~"Students vs Non-Students")+
            geom_bar(stat="identity", width=0.4, position=position_dodge(0.7))+
            geom_text(aes(label=Count), vjust=-0.2, colour="black")+
            guides(fill=F)

grid.arrange(plot1, plot2, ncol=2)
######################################################################################
plot3 <- dplyr::filter(MainData, student == "Yes") %>%
              data.frame(table(.$default)) %>%
              dplyr::group_by(default) %>%
              dplyr::count(default) %>%
              dplyr::rename(Count = "n") %>%
              ggplot(aes(x=default, y=Count, fill = default))+
              xlab("Default Status")+ ylab("Number of Students")+
              facet_wrap(~"Default and NonDefaulters in students")+
              geom_bar(stat="identity", width=0.4, position=position_dodge(0.7))+
              geom_text(aes(label=Count), vjust=-0.2, colour="black")+
              guides(fill=F)
plot4 <- dplyr::filter(MainData, student == "No") %>%
              data.frame(table(.$default)) %>%
              dplyr::group_by(default) %>%
              dplyr::count(default) %>%
              dplyr::rename(Count = "n") %>%
              ggplot(aes(x=default, y=Count, fill = default))+
              xlab("Default Status")+ ylab("Number of Students")+
              facet_wrap(~"Default and NonDefaulters in NonStudents")+
              geom_bar(stat="identity", width=0.4, position=position_dodge(0.7))+
              geom_text(aes(label=Count), vjust=-0.2, colour="black")+
              guides(fill=F)
grid.arrange(plot3, plot4, ncol=2)

#####################################################################################
means <- aggregate(balance ~  default, MainData, mean)
plot5 <- ggplot(MainData, aes(x = as.factor(default), y=balance, fill = default))+
            xlab("Default Status")+ ylab("Available balance")+ 
            facet_wrap(~"Balance for Defaulters vs NonDefaulters")+
            geom_boxplot()+
            guides(fill=F)+
            geom_text(data = means, aes(label = balance, y = balance + 0.04))

means1 <- aggregate(income ~  default, MainData, mean)
plot6 <- ggplot(MainData, aes(x = as.factor(default), y=income, fill = default))+
            xlab("Default Status")+ ylab("Income")+ 
            facet_wrap(~"Income for Defaulters vs NonDefaulters")+
            geom_boxplot()+
            guides(fill=F)+
            geom_text(data = means1, aes(label = income, y = income + 0.04))
grid.arrange(plot5, plot6, ncol=2)
#########################################################################################
means <- aggregate(balance ~  student, MainData, mean)
plot7 <- ggplot(MainData, aes(x = as.factor(student), y=balance, fill = student))+
            xlab("Student")+ ylab("Available balance")+ 
            facet_wrap(~"Balance for Students vs Non-Students")+
            geom_boxplot()+
            guides(fill=F)+
            geom_text(data = means, aes(label = balance, y = balance + 0.04))

means1 <- aggregate(income ~  student, MainData, mean)
plot8 <- ggplot(MainData, aes(x = as.factor(student), y=income, fill = student))+
            xlab("Student")+ ylab("Income")+ 
            facet_wrap(~"Income for Students vs Non-Students")+
            geom_boxplot()+
            guides(fill=F)+
            geom_text(data = means1, aes(label = income, y = income + 0.04))
grid.arrange(plot7, plot8, ncol=2)
#########################################################################################
# Let Us Start Machine Learning
## Data Preparation
# Recode the default and student column
MainData$default <-ifelse(MainData$default == "No", 0,1)
MainData$default <-ifelse(MainData$student == "No", 0,1)
# Convert the default and student columns into factor variables
MainData$default <-as.factor(MainData$default)
MainData$default <-as.factor(MainData$student)

# Replace balance of 0 with 1
MainData$balance[MainData$balance == 0] <- 1

# Drop the unique Id column
MainData <- MainData[,-1]
##################################################################################
##Feature Egineering
MainData <- dplyr::mutate(MainData, IncomeBalance = income - balance) %>%
                dplyr::mutate(DividebyBalance = income / balance) %>%
                dplyr::mutate(DividebyIncome = balance / income) %>%
                dplyr::mutate(IncomeRatio = income / DividebyBalance) %>%
                dplyr::mutate(IncomeBalanceRatio = IncomeBalance / DividebyBalance) %>%
                dplyr::mutate(IncomeIncomeRatio = IncomeBalance / DividebyIncome) %>%
                dplyr::mutate(BalanceBalanceRatio = balance / DividebyBalance) %>%
                dplyr::mutate(BalanceIncomeRatio = balance / DividebyIncome)
#View(MainData)
# split into train and test set
set.seed(113)

# Remove near zero variance predictors
# Remove near zero variance predictors
variances<-apply(MainData, 2, var)
variances 
remove_cols <- variances[which(variances<=0.0025)]
remove_cols
# one column DividebyIncome is a near zero variance predictor

# Remove columns with near zero variance predictors MainData
MainData <- MainData[ , setdiff(names(MainData), "DividebyIncome")]

cutoff <- createDataPartition(MainData$default, p = 0.75, list = FALSE)
train <- MainData[cutoff,]
test <- MainData[-cutoff,]
names(train)

# Remove Multicollinearity
ncol(train)
descrCorr <- cor(train[,4:11])
highCorr <- findCorrelation(descrCorr, 0.90)
# Drop highly corrlated columns i.e the student, IncomeBalanceRatio, income,
# and DividebyBalance columns
train[, highCorr]
if (length(highCorr) != 0){
  train <- train[, -highCorr]
  test <- test[, -highCorr]
}



#####################################################
# Tune Model using GBM
gbmGrid <-  expand.grid(interaction.depth = 3,
                        n.trees = c(200, 198, 202),
                        shrinkage = 0.01,
                        n.minobsinnode = c(33, 35, 34))

gbmModel<- caret::train(default~., data = train, method = "gbm",
                        preProcess = c("pca"),
                        metric="AUC",
                        trControl= trainControl(method = "repeatedcv",
                                                number = 10,
                                                repeats = 10,
                                                verboseIter = FALSE,
                                                classProbs=TRUE,
                                                summaryFunction=mnLogLoss),
                        tuneGrid = gbmGrid)

summary(gbmModel)
ggplot(gbmModel) + theme(legend.position = "top")
make_prediction <- predict.train(gbmModel, newdata = test, type = "raw")
gbmPrediction <- confusionMatrix(make_prediction, test$default)

#####################################################################################
# Tune Model using rpart
rpartModel<- train(default~., data = train, method = "rpart",
                 metric = "AUC",
                 trControl= trainControl(method = "repeatedcv",
                                         number = 10,
                                         classProbs = TRUE,
                                         summaryFunction=prSummary,
                                         verboseIter = FALSE,
                                         allowParallel = TRUE))
summary(rpartModel)
# ggplot(rpartModel) + theme(legend.position = "top")

make_prediction1 <- predict.train(rpartModel, newdata = test, type = "raw")
rpartPrediction <- confusionMatrix(make_prediction1, test$default)

#####################################################################################
xgbmGrid = expand.grid(nrounds = 20,
                       max_depth = 1,
                       eta = 0.3,
                       gamma = c(2.4, 2.3, 2.2),
                       colsample_bytree = c(0.9, 0.8, 0.7),
                       min_child_weight = 1,
                       subsample = 0.75)
xgbmModel<- caret::train(default~., data = train, method = "xgbTree",
                               preProcess = c("center", "scale", 'pca'),
                               metric="AUC",
                               trControl= trainControl(method = "repeatedcv",
                                                       number = 10,
                                                       repeats = 10,
                                                       verboseIter = FALSE,
                                                       classProbs=TRUE,
                                                       summaryFunction=prSummary),
                               tuneGrid = xgbmGrid)

make_prediction2 <- predict.train(xgbmModel, newdata = test, type = "raw")
xgbmPrediction <- confusionMatrix(make_prediction2, test$default)

###########################################################3
statistic <- c("Sensitivity", "Specificity", "Pos Pred Value", "Neg Pred Value", 
                  "Precision","Recall", "F1 Score", "Prevalence", "Detection Rate",
                  "Detection Prevalence", "Balanced Accuracy") 
GBMModel <- as.vector(gbmPrediction$byClass)
RPARTMODEL <- as.vector(rpartPrediction$byClass)
XGBOOSTMODEL <- as.vector(xgbmPrediction$byClass)
Compare.Models <-data.frame(cbind(GBMModel, RPARTMODEL, XGBOOSTMODEL)) %>%
                    mutate_if(is.factor, as.numeric) %>%
                    data.frame(cbind(statistic)) %>%
                    dplyr::select(statistic, everything()) %>%
                    format(., digits = 4)

kable(Compare.Models) %>%
  kable_styling(bootstrap_options=c("striped", "hover"), full_width = F, position="left") %>%
  row_spec(0, bold = T, color = "white", background = "#3399f3")