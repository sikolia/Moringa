# Moringa: Let us Play with the Default Rate Dataset
In order to reproduce this project, ensure that you have rstudio, r, and git installed. Clone this repository and open the index.rmd file using rstudio. Click the knit icon in rstudio to produce the output that is available [here](https://cycks.github.io/Moringa)

# Author
Wycliffe Atswenje Sikolia

# License
[MIT](https://opensource.org/licenses/MIT)
